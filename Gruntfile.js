module.exports = function(grunt) {
  grunt.initConfig({
    //Watch Contrib Task
    watch: {
      sass: {
        files: [
          "view/sass/*.scss",
          "view/sass/components/*.scss",
          "view/sass/components/forms/*.scss"
        ],
        tasks: ["sass"],
        options: {
          livereload: true
        }
      }
    },
    //Sass Task
    sass: {
      dev: {
        options: {
          style: "expanded"
        },
        files: {
          "view/css/material.css": "view/sass/materialize.scss"
        }
      }
    },
    //BrowserSync Task
    browserSync: {
      dev: {
        bsFiles: {
          src: [
            "cntrl/*php",
            "core/*php",
            "mdl/*php",
            "view/*php",
            "view/foot/*.php",
            "view/head/*.php",
            "view/css/*.css"
          ]
        },
        options: {
          watchTask: true,
          proxy: "localhost/xubastas"
        }
      }
    }
  });
  //load tasks
  grunt.loadNpmTasks("grunt-contrib-sass");
  grunt.loadNpmTasks("grunt-contrib-watch");
  grunt.loadNpmTasks("grunt-browser-sync");

  // define tasking on comands
  grunt.registerTask("default", ["browserSync", "watch"]); // grun sass compile Sass
  //grunt.registerTask('monitor', ['browserSync']); // grunt compile browserSync
};
