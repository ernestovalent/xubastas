-- MySQL Script generated by MySQL Workbench
-- Sun Jan 26 14:09:49 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema xubastas
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema xubastas
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `xubastas` DEFAULT CHARACTER SET utf8 ;
USE `xubastas` ;

-- -----------------------------------------------------
-- Table `xubastas`.`udn`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`udn` (
  `udn_id` TINYINT NOT NULL AUTO_INCREMENT,
  `Nombre` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`udn_id`),
  UNIQUE INDEX `id_UNIQUE` (`udn_id` ASC))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`usuario` (
  `usuario_id` INT NOT NULL AUTO_INCREMENT,
  `estatus` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0=suspen, 1=admin, 2=gestor, 3=colabor',
  `correo` VARCHAR(70) NOT NULL,
  `pass` VARCHAR(150) NOT NULL,
  `nombre` VARCHAR(150) NULL,
  `numero_colaborador` VARCHAR(10) NULL,
  `telefono` VARCHAR(10) NULL,
  `udn` TINYINT NULL,
  `foto` VARCHAR(250) NULL,
  `denuncia` TINYINT(1) NULL DEFAULT 0,
  `fecha_registrado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_suspendido` TIMESTAMP NULL,
  `recuperacion` VARCHAR(36) NULL,
  PRIMARY KEY (`usuario_id`),
  UNIQUE INDEX `id_UNIQUE` (`usuario_id` ASC),
  UNIQUE INDEX `numero_colaborador_UNIQUE` (`numero_colaborador` ASC),
  INDEX `FK_Usuario_UDN_idx` (`udn` ASC),
  CONSTRAINT `FK_Usuario_UDN`
    FOREIGN KEY (`udn`)
    REFERENCES `xubastas`.`udn` (`udn_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`solicitud`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`solicitud` (
  `solicitud_id` INT NOT NULL AUTO_INCREMENT,
  `creador` INT NOT NULL,
  `autoriza` INT NULL,
  `estatus` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0=solicitado, 1=aceptado',
  `correo` VARCHAR(70) NOT NULL,
  `nombre` VARCHAR(150) NULL,
  `numero_colaborador` VARCHAR(10) NULL,
  `telefono` VARCHAR(10) NULL,
  `udn` TINYINT NULL,
  `fecha_registrado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_autorizado` TIMESTAMP NULL,
  PRIMARY KEY (`solicitud_id`),
  UNIQUE INDEX `id_UNIQUE` (`solicitud_id` ASC),
  INDEX `FK_Solicitud_Usuario_idx` (`creador` ASC),
  INDEX `FK_Solicitud_Udn_idx` (`udn` ASC),
  INDEX `FK_Solicitud_Autoriza_idx` (`autoriza` ASC),
  CONSTRAINT `FK_Solicitud_Usuario`
    FOREIGN KEY (`creador`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Solicitud_Udn`
    FOREIGN KEY (`udn`)
    REFERENCES `xubastas`.`udn` (`udn_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Solicitud_Autoriza`
    FOREIGN KEY (`autoriza`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`Subasta`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`Subasta` (
  `subasta_id` INT NOT NULL AUTO_INCREMENT,
  `estatus` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '0=suspendido, 1=no publicado, 2=publicado',
  `creador` INT NULL,
  `udn` TINYINT NULL,
  `nombre` VARCHAR(255) NOT NULL,
  `descripcion` TEXT(2000) NULL,
  `fecha_inicio` TIMESTAMP NULL,
  `fecha_fin` TIMESTAMP NULL,
  `lugar_entrega` TEXT(1000) NULL,
  `fecha_registro` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_suspendido` TIMESTAMP NULL,
  `motivo_suspendido` TEXT(1000) NULL,
  PRIMARY KEY (`subasta_id`),
  INDEX `FK_Subasta_Creador_idx` (`creador` ASC),
  UNIQUE INDEX `id_UNIQUE` (`subasta_id` ASC),
  INDEX `FK_Subasta_Udn_idx` (`udn` ASC),
  CONSTRAINT `FK_Subasta_Creador`
    FOREIGN KEY (`creador`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Subasta_Udn`
    FOREIGN KEY (`udn`)
    REFERENCES `xubastas`.`udn` (`udn_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`articulo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`articulo` (
  `articulo_id` INT NOT NULL AUTO_INCREMENT,
  `creador` INT NULL,
  `subasta` INT NULL,
  `estatus` TINYINT(1) NOT NULL DEFAULT 1 COMMENT '0=suspendido, 1= no publicado, 2= publicado, 3= agotado',
  `nombre` VARCHAR(255) NOT NULL,
  `descripcion` TEXT(2000) NULL,
  `precio_inicial` DECIMAL(10,2) NOT NULL DEFAULT 1,
  `escalabilidad` DECIMAL(10) NULL DEFAULT 1,
  `img1` VARCHAR(255) NULL,
  `img2` VARCHAR(255) NULL,
  `img3` VARCHAR(255) NULL,
  `img4` VARCHAR(255) NULL,
  `img5` VARCHAR(255) NULL,
  `fecha_registrado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `fecha_suspendido` TIMESTAMP NULL,
  `motivo_suspendido` TEXT(1000) NULL,
  PRIMARY KEY (`articulo_id`),
  INDEX `FK_Articulo_Subasta_idx` (`subasta` ASC),
  INDEX `FK_Articulo_Usuario_idx` (`creador` ASC),
  UNIQUE INDEX `id_UNIQUE` (`articulo_id` ASC),
  CONSTRAINT `FK_Articulo_Subasta`
    FOREIGN KEY (`subasta`)
    REFERENCES `xubastas`.`Subasta` (`subasta_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Articulo_Usuario`
    FOREIGN KEY (`creador`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`puja`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`puja` (
  `articulo` INT NOT NULL,
  `usuario` INT NOT NULL,
  `cantidad` DECIMAL(10,2) NOT NULL DEFAULT 0,
  `fecha_registrado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  INDEX `FK_Puja_Articulo_idx` (`articulo` ASC),
  INDEX `FK_Puja_Usuario_idx` (`usuario` ASC),
  CONSTRAINT `FK_Puja_Articulo`
    FOREIGN KEY (`articulo`)
    REFERENCES `xubastas`.`articulo` (`articulo_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Puja_Usuario`
    FOREIGN KEY (`usuario`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`notificacion`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`notificacion` (
  `notificacion_id` BIGINT NOT NULL AUTO_INCREMENT,
  `accion` TINYINT(1) NOT NULL COMMENT '1=registra, 2=modifica, 3=suspende',
  `propiedad` TINYINT(1) NOT NULL COMMENT '1=usuarios, 2=subasta, 3=articulo, 4=puja, 5=udn',
  `objeto` INT NOT NULL,
  `creador` INT NOT NULL,
  `receptor` INT NOT NULL,
  `visto` TINYINT(1) NULL DEFAULT 0,
  `url` VARCHAR(250) NULL,
  `fecha_registrado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`notificacion_id`),
  UNIQUE INDEX `id_UNIQUE` (`notificacion_id` ASC),
  INDEX `FK_Notificacion_Genera_idx` (`creador` ASC),
  INDEX `FK_Notificacion_Recibe_idx` (`receptor` ASC),
  CONSTRAINT `FK_Notificacion_Genera`
    FOREIGN KEY (`creador`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Notificacion_Recibe`
    FOREIGN KEY (`receptor`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `xubastas`.`denuncia`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `xubastas`.`denuncia` (
  `denuncia_id` INT NOT NULL AUTO_INCREMENT,
  `estatus` TINYINT(1) NOT NULL DEFAULT 0 COMMENT '0=creado 1=aprobado',
  `creador` INT NULL,
  `denunciado` INT NOT NULL,
  `autoriza` INT NULL,
  `motivo` TEXT(1000) NULL,
  `fecha_registrado` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`denuncia_id`),
  INDEX `FK_Denuncia_Solicita_idx` (`creador` ASC),
  INDEX `FK_Denuncia_Autoriza_idx` (`autoriza` ASC),
  INDEX `FK_Denuncia_Denunciado_idx` (`denunciado` ASC),
  CONSTRAINT `FK_Denuncia_Solicita`
    FOREIGN KEY (`creador`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Denuncia_Autoriza`
    FOREIGN KEY (`autoriza`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `FK_Denuncia_Denunciado`
    FOREIGN KEY (`denunciado`)
    REFERENCES `xubastas`.`usuario` (`usuario_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `xubastas`;

DELIMITER $$
USE `xubastas`$$
CREATE DEFINER = CURRENT_USER TRIGGER `xubastas`.`usuario_BEFORE_INSERT` BEFORE INSERT ON `usuario` FOR EACH ROW
BEGIN
	SET NEW.recuperacion = replace(uuid(),'-','');
END$$

USE `xubastas`$$
CREATE DEFINER = CURRENT_USER TRIGGER `xubastas`.`usuario_BEFORE_UPDATE` BEFORE UPDATE ON `usuario` FOR EACH ROW
BEGIN
	IF NEW.denuncia >= 3 THEN
		SET NEW.estatus = 0;
	END IF;
	SET NEW.recuperacion = replace(uuid(),'-','');
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
